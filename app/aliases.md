# zsh aliases

# GDK
alias gdk.ce.run="cd ~/git/gitlab/gdk-ce/ && gdk run"
alias gdk.ee.run="cd ~/git/gitlab/gdk-ee/ && gdk run"
alias gdk.geo.run="cd ~/git/gitlab/gdk-geo/ && gdk run"