Default public GitLab example project hosted on [GitLab.com](https://gitlab.com/akaemmerle/example-project).

For open topics, see this project's [issue board](https://gitlab.com/akaemmerle/example-project/boards/592158?=).  
Feel free to provide any feedback via a [new issue](https://gitlab.com/akaemmerle/example-project/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

# Contributing

1. Create an [issue](https://gitlab.com/akaemmerle/akaemmerle.gitlab.io/issues/new) or [merge request](https://gitlab.com/akaemmerle/akaemmerle.gitlab.io/merge_requests/new)
2. Assign it to @akaemmerle
